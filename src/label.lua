Label = Object.extend(Object)

function Label:new(x, y, size, text)
    self.x = x
    self.y = y
    self.text = text
    self.font = love.graphics.newFont(size, "normal", love.graphics.getDPIScale())
end

function Label:draw()
    love.graphics.setColor(1.0, 1.0, 1.0)
    love.graphics.print(self.text, self.font, self.x, self.y)
end

function Label:setText(text)
    self.text = text
end