function love.load()
    Object = require "classic"
    require "menubutton"
    require "game"
    require "menu"
    require "endscreen"
    screenWidth, screenHeight, _ = love.window.getMode()
    currentScreen = "menu"
    game = Game()
    menu = Menu()
    endScreen = EndScreen()
end

function love.update(dt)
    if currentScreen == "menu" then
        menu:update(dt)
    elseif currentScreen == "end" then
        endScreen:update(dt)
    else
        game:update(dt)
    end
end

function love.draw()
    if currentScreen == "menu" then
        menu:draw()
    elseif currentScreen == "end" then
        endScreen:draw()
    else
        game:draw()
    end
end

function love.keypressed(key)
    if currentScreen == "game" then
        game:keyPressed(key)
    end
end