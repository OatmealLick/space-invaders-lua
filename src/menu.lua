Menu = Object:extend()

function Menu:new()
    local buttonWidth = 220
    local buttonHeight = 80
    self.playButton = MenuButton(screenWidth / 2, screenHeight * 0.3, buttonWidth, buttonHeight, 25, "Play")
    self.quitButton = MenuButton(screenWidth / 2, screenHeight * 0.5, buttonWidth, buttonHeight, 25, "Quit")
end

function Menu:update(dt)
    if self.playButton:isClicked() then
        game:restart()
        currentScreen = "game"
    elseif self.quitButton:isClicked() then
        love.window.close()
    end
end

function Menu:draw()
    self.playButton:draw()
    self.quitButton:draw()
end

function Menu:keyPressed(key)

end