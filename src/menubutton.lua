MenuButton = Object.extend(Object)

function MenuButton:new(centerX, centerY, width, height, size, text)
    self.x = centerX - width / 2
    self.y = centerY - height / 2
    self.width = width
    self.height = height
    self.text = text
    self.size = size
    self.font = love.graphics.newFont(size, "normal", love.graphics.getDPIScale())
    self.clicked = false
end

function MenuButton:draw()
    love.graphics.setColor(0.5, 0.85, 0.5)
    love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
    love.graphics.setColor(0.1, 0.1, 0.1)
    love.graphics.print(self.text, self.font, self.x + self.size, self.y + 0.3 * self.height)
end

function MenuButton:isClicked()
    if love.mouse.isDown(1) and self.clicked == false then
        self.clicked = true
        local x, y = love.mouse.getX(), love.mouse.getY()
        return self:isPositionInsideButton(x, y)
    end
    if not love.mouse.isDown(1) then
        self.clicked = false
    end
    return false
end

function MenuButton:isPositionInsideButton(x, y)
    return (x > self.x and x < self.x + self.width) and (y > self.y and y < self.y + self.height)
end