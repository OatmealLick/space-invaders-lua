--! file: invaders.lua

Invaders = Object.extend(Object)

function Invaders:new(horizontalCount, verticalCount, health)
    horizontalMargin = 10
    verticalMargin = 5
    self.timeToLower = 5.0
    self.currentTimeToLower = self.timeToLower
    self.timeToShoot = 2.0
    self.currentTimeToShoot = self.timeToShoot
    self.horizontalCount = horizontalCount
    self.verticalCount = verticalCount
    local predictedWidth = (0.6 * screenWidth) / self.horizontalCount - horizontalMargin
    self.defaultInvaderWidth = math.min(predictedWidth, 70)
    self.defaultInvaderHeight = self.defaultInvaderWidth
    self.invaders = {}
    for i = 1, self.verticalCount do
        self.invaders[i] = {}
        for j = 1, self.horizontalCount do
            posX = j * (self.defaultInvaderWidth + horizontalMargin) + horizontalMargin
            posY = i * (self.defaultInvaderHeight + verticalMargin) + verticalMargin
            self.invaders[i][j] = Invader(posX, posY, self.defaultInvaderWidth, self.defaultInvaderHeight, health)
        end
    end
end

function Invaders:update(dt)
    self.currentTimeToLower = self.currentTimeToLower - dt
    local shouldLower = self.currentTimeToLower < 0
    if shouldLower then
        self.currentTimeToLower = self.timeToLower
    end

    self.currentTimeToShoot = self.currentTimeToShoot - dt
    local shouldShoot = self.currentTimeToShoot < 0
    if shouldShoot then
        self.currentTimeToShoot = self.timeToShoot
        self:shoot()
    end

    local shouldChangeDir = self:xPositionOfRightmost() > 700 or self:xPositionOfLeftmost() < 50
    for i = 1, self.verticalCount do
        for j = 1, self.horizontalCount do
            local invader = self.invaders[i][j]
            if shouldChangeDir then
                invader:changeDir()
            end
            if shouldLower then
                invader.y = invader.y + self.defaultInvaderHeight
            end
            invader:update(dt)

            if invader.y > 400 then
                print("YOU LOST")
                love.load()
            end
        end
    end
end

function Invaders:draw()
    for i = 1, self.verticalCount do
        for j = 1, self.horizontalCount do
            self.invaders[i][j]:draw()
        end
    end
end

function Invaders:shoot()
    local invader = self:selectShooter()
    invader:shoot()
end

function Invaders:selectShooter()
    local availableToShoot = {}
    for i = 1, self.horizontalCount do
        for j = self.verticalCount, 1, -1 do
            local invader = self.invaders[j][i]
            if not invader.dead then
                table.insert(availableToShoot, invader)
                print("Added invader of row " .. j .. " and col " .. i)
                break
            end
        end
    end

    print("Having " .. #availableToShoot .. " invaders to choose from")
    local selected = math.random(1, #availableToShoot)
    return availableToShoot[selected]
end

function Invaders:xPositionOfRightmost()
    local rightmostX = 1
    for i = self.verticalCount,1,-1 do
        for j = self.horizontalCount,1,-1 do
            if self.invaders[i][j].x > rightmostX then
                rightmostX = self.invaders[i][j].x
            end
        end
    end
    return rightmostX
end

function Invaders:xPositionOfLeftmost()
    local leftmostX = 800
    for i = 1, self.verticalCount do
        for j = 1, self.horizontalCount do
            if self.invaders[i][j].x < leftmostX then
                leftmostX = self.invaders[i][j].x
            end
        end
    end
    return leftmostX
end

function Invaders:areAllDead()
    local dead = true
    for i = 1, self.verticalCount do
        for j = 1, self.horizontalCount do
            if not self.invaders[i][j].dead then
                dead = false
            end
        end
    end
    return dead
end