Player = Object:extend()

function Player:new(x, y)
    self.x = x
    self.y = y
    self.width = 30
    self.height = 30
    self.speed = 300
    self.health = 3
end

function Player:update(dt)
    if love.keyboard.isDown("left") then
        self.x = self.x - self.speed * dt
    elseif love.keyboard.isDown("right") then
        self.x = self.x + self.speed * dt
    end
end

function Player:draw()
    love.graphics.setColor(0.7, 0.4, 0.4)
    love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
end

function Player:keyPressed(key)
    if key == "space" then
        table.insert(listOfBullets, Bullet(self.x, self.y, true))
    end
end

function Player:takeHit()
    print("got hit")
    if self.health == 1 then
        currentScreen = "end"
        print("YOU LOST")
    else
        self.health = self.health - 1
        healthLabel:setText("Health: " .. self.health)
    end
end