Bullet = Object:extend()

function Bullet:new(x, y, fromPlayer)
    self.image = love.graphics.newImage("bullet.png")
    self.x = x
    self.y = y
    if fromPlayer then
        self.speed = 700
    else
        self.speed = -300
    end
    self.width = self.image:getWidth()
    self.height = self.image:getHeight()
end

function Bullet:update(dt)
    if self.dead then
        return
    end
    self.y = self.y - self.speed * dt
end

function Bullet:draw()
    if not self.dead then
        love.graphics.setColor(1.0, 1.0, 1.0)
        love.graphics.draw(self.image, self.x, self.y)
    end
end

function Bullet:doCheckCollision(obj)
    if self.dead then
        return false
    end
    local self_left = self.x
    local self_right = self.x + self.width
    local self_top = self.y
    local self_bottom = self.y + self.height

    local obj_left = obj.x
    local obj_right = obj.x + obj.width
    local obj_top = obj.y
    local obj_bottom = obj.y + obj.height

    if self_right > obj_left and
            self_left < obj_right and
            self_bottom > obj_top and
            self_top < obj_bottom then
        return true
    end
    return false
end

function Bullet:checkCollision(invaders)
    for i = 1, invaders.verticalCount do
        for j = 1, invaders.horizontalCount do
            local invader = invaders.invaders[i][j]
            if not invader.dead then
                local collided = self:doCheckCollision(invaders.invaders[i][j])
                if collided then
                    invaders.invaders[i][j]:die()
                    return true
                end
            end
        end
    end
    return false
end

function Bullet:die()
    self.dead = true
end