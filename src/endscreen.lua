EndScreen = Object:extend()

function EndScreen:new()
    local buttonWidth = 220
    local buttonHeight = 80
    self.endGameLabel = Label(screenWidth * 0.3, screenHeight * 0.3, 60, "YOU LOST")
    self.playButton = MenuButton(screenWidth / 2, screenHeight * 0.7, buttonWidth, buttonHeight, 25, "Back to menu")
end

function EndScreen:update(_)
    if self.playButton:isClicked() then
        currentScreen = "menu"
    end
end

function EndScreen:draw()
    self.playButton:draw()
    self.endGameLabel:draw()
end