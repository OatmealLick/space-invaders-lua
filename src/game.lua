Game = Object:extend()

function Game:new()
    require "invader"
    require "invaders"
    require "player"
    require "bullet"
    require "label"
    require "level"
    self.menuButton = MenuButton(0.9 * screenWidth, 0.05 * screenHeight, 50, 30, 12, "Menu")
    self.pauseButton = MenuButton(0.83 * screenWidth, 0.05 * screenHeight, 50, 30, 12, "Pause")
    self.levels = {}

    table.insert(self.levels, Level(6, 2, 1))
    table.insert(self.levels, Level(9, 4, 1))
    table.insert(self.levels, Level(8, 3, 2))
    self:restart()
end

function Game:restart()
    self.score = 0
    self.player = Player(0.5 * screenWidth, 0.88 * screenHeight)
    self.level = 1
    local currentLevel = self.levels[self.level]
    self.invaders = Invaders(currentLevel.width, currentLevel.height, currentLevel.health)
    listOfBullets = {}
    invadersBullets = {}
    self.paused = false
    healthLabel = Label(0.02 * screenWidth, 0.02 * screenHeight, 12, "Health: " .. self.player.health)
    levelLabel = Label(0.02 * screenWidth, 0.04 * screenHeight, 12, "Level: " .. self.level)
    scoreLabel = Label(0.02 * screenWidth, 0.06 * screenHeight, 12, "Score: " .. self.score)
end

function Game:nextLevel()
    self.player = Player(0.5 * screenWidth, 0.88 * screenHeight)
    self.level = self.level + 1
    local currentLevel = self.levels[self.level]
    self.invaders = Invaders(currentLevel.width, currentLevel.height, currentLevel.health)
    listOfBullets = {}
    invadersBullets = {}
    self.paused = false
end

function Game:update(dt)
    if not self.paused then
        self.player:update(dt)
        self.invaders:update(dt)

        for i,v in ipairs(listOfBullets) do
            v:update(dt)
            local collided = v:checkCollision(self.invaders)
            if collided then
                v:die()
                table.remove(listOfBullets, i)
                self.score = self.score + 1
                scoreLabel:setText("Score: " .. self.score)
            end
        end

        for i,v in ipairs(invadersBullets) do
            v:update(dt)
            local collided = v:doCheckCollision(self.player)
            if collided then
                v:die()
                self.player:takeHit()
                table.remove(invadersBullets, i)
            end
        end

        if self.invaders:areAllDead() then
            self:nextLevel()
        end
    end

    if self.menuButton:isClicked() then
        currentScreen = "menu"
    end

    if self.pauseButton:isClicked() then
        self.paused = not self.paused
    end
end

function Game:draw()
    self.player:draw()
    self.invaders:draw()

    for i,v in ipairs(listOfBullets) do
        v:draw()
    end

    for i,v in ipairs(invadersBullets) do
        v:draw()
    end

    self.menuButton:draw()
    self.pauseButton:draw()
    healthLabel:draw()
    levelLabel:draw()
    scoreLabel:draw()
end

function Game:keyPressed(key)
    self.player:keyPressed(key)
end

