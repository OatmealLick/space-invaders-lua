--! file: invader.lua

-- Pass Object as first argument.
Invader = Object.extend(Object)

function Invader:new(x, y, width, height, health)
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    self.speed = 60
    self.dir = 1
    self.dead = false
    self.health = health
end

function Invader:update(dt)
    self.x = self.x + self.speed * self.dir * dt
end

function Invader:draw()
    if not self.dead then
        if self.health > 1 then
            love.graphics.setColor(0.4, 0.4, 0.4)
        else
            love.graphics.setColor(0.3, 0.4, 0.85)
        end
        love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
    end
end

function Invader:changeDir()
    self.dir = -1 * self.dir
end

function Invader:die()
    if self.health == 1 then
        self.dead = true
    else
        self.health = self.health - 1
    end
end

function Invader:shoot()
    table.insert(invadersBullets, Bullet(self.x, self.y, false))
end